<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH . 'assets/lib/PSWebServiceLibrary.php');

class FuncionesWebservicePrestashop
{

  public function __construct(){ }

  public function index(){ }

  public function saludar(){
    return 'esta respuesta ';
  }


  public function BuscarDatosPrestashop($resource, $display,$filter, $datoFiltro )
{
    try {
        // creating web service access
        $webService = new PrestaShopWebservice(
            'http://www.unico.com.co/',
            'GZ9DRYMR64NSDGH7MRZ1VARL77AA37DL',
            false
        );
 

   $xml = $webService->get(array('resource' => $resource, 'display' => $display , $filter => $datoFiltro));

        $json     = json_encode($xml);
        $respuesta = json_decode($json, true);       
        return $respuesta;

        // print("<pre>" . print_r($respuesta, true) . "</pre>");
    } catch (PrestaShopWebserviceException $ex) {
        $trace = $ex->getTrace(); // Retrieve all information on the error
        $errorCode = $trace[0]['args'][0]; // Retrieve the error code
        if ($errorCode == 401)
            var_dump('Bad auth key');
        else
            var_dump('Other error : <br />' . $ex->getMessage());
        // Shows a message related to the error
    }
}



}

/* End of file FuncionesWebservicePrestashop.php */
/* Location: ./application/libraries/FuncionesWebservicePrestashop.php */